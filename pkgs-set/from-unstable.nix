pkgs: {
  inherit
    (pkgs)
    helix
    alejandra
    rnix-lsp
    chromium
    rofi-wayland
    vscode
    ;
}
